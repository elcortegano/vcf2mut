## README

A few Cython libraries to find mutations in VCF files, and to get genomic context and features from them.

# Build libraries

Python modules can be built from the cython code provided simply by running:

```
python setup.py build_ext --inplace
```

Note that some dependencies are required, including [Cython](https://cython.org/) and [cyvcf2](https://github.com/brentp/cyvcf2).
