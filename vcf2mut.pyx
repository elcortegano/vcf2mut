import cyvcf2
import numpy as np
import os
import sys
import pandas
import functools
import warnings

# Find single-base mutations
def findMut(vcfile, minQUAL=150, minMQ=50, minDPm=6, minDPt=150, minGQ = 0, excludeINDELS='context', excludeNOISE='context', context=10, nlines=27, mode="gatk", out=None, contig=''):

    HERE = os.path.dirname(__file__)
    VCF_PATH = os.path.join(HERE, vcfile)
    vcf = cyvcf2.VCF(VCF_PATH)

    LINES = vcf.samples
    LQ = 0.17 # loq and high allele frequency (for considering a site polimorphic or not)
    HQ = 1.0-LQ
    min_lines = 3 # minimum number of lines whith quality parameters
    data = []
    count_indel = 0
    count_noise = 0
    NA = "."
    if mode != "gatk" and mode != "freebayes":
        print("Error, unknown mode")
        sys.error(-1)

    # Mutation identification
    for v in vcf(contig):
        
        CHROM = v.CHROM
        POS = v.POS
        QUAL = v.QUAL
        if QUAL is None or QUAL == float('inf') or QUAL == float('-inf') or not isinstance (QUAL, (float, int)): QUAL = NA
        else: QUAL = round(QUAL,2)
        MQ = v.INFO.get('MQ')
        if mode == "freebayes":
            if len(v.ALT) > 0: MQ = v.INFO.get('MQM')
            else: MQ = v.INFO.get('MQMR')
        if not isinstance (MQ, (float, int)): MQ = NA
        else: MQ = round(MQ, 2)
        DP = v.INFO.get('DP')
        GQ = v.format('GQ')
        if GQ is None: GQ = v.format('RGQ')
        if GQ is None: GQ = NA
        mGQ = GQ
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            if mGQ is not NA: mGQ = round(np.nanmean(GQ[GQ>=0]),2)
        CALL = False
        MUT = False
        ALT = NA
        if len(v.ALT) > 0: ALT = v.ALT[0]
        LINE = NA
        site = [CHROM, POS, v.REF, ALT, CALL, MUT, LINE, QUAL, MQ, DP, mGQ]

        # Determine "noisy" positions
        alt_depth = True
        noise = False
        if v.gt_alt_depths[0] == -1:
            alt_depth = False
        if alt_depth:
            dq = np.divide(v.gt_alt_depths, v.gt_alt_depths+v.gt_ref_depths)
            noise = functools.reduce(lambda count, x: count + (x>LQ and x<HQ), dq, 0)
            noise = noise > (nlines/2.0)
        # Exclude positions based on context
        if excludeNOISE=='context' and noise and len(data) > context:
            count_noise = context
            L = len(data)
            c = 1
            while context >= c:
                if data[L-c][0] != CHROM: break
                elif POS - data[L-c][1] > context: break
                else:
                    data[-c][4] = False
                    data[-c][5] = False
                    c = c+1
        if excludeINDELS=='context' and count_indel > 0:
            count_indel = count_indel - 1
            data.append(site)
            continue
        elif excludeNOISE=='context' and count_noise > 0:
            count_noise = count_noise - 1
            data.append(site)
            continue

        # Minimum PHRED called site quality (QUAL), mapping quality (MQ) and combined read depth (DP)
        if MQ == NA or MQ < minMQ:
            data.append(site)
            continue
        if DP < minDPt:
            data.append(site)
            continue

        # At least two sequences must have confident genotype calls 
        if GQ is NA or len(GQ[GQ>=minGQ]) < min_lines:
            data.append(site)
            continue
        
        # All lines must be called as haploids, and no lines can be called as heterozygote
        GT = v.gt_types
        if np.any(GT == 1) or v.ploidy != 1 or v.num_het > 0: # heterozygote lines
            data.append(site)
            continue
        elif np.any(GT == 2): # non-genotyped lines
            tGT = np.unique(GT, return_counts=True)
            unk = np.where(tGT[0]==2)[0][0]
            if tGT[1][unk] > (nlines-min_lines):
                data.append(site)
                continue

        # All lines with mapped reads have the same genotype call (excluding potential mutants)
        B = v.gt_bases
        nB = np.unique(B[B!=NA]).size
        if nB > 2 or nB == 0:
            data.append(site)
            continue

        tB = np.unique(B[B!='.'], return_counts=True)
        if tB[1].size == 2 and tB[1][0] != 1 and tB[1][1] != 1:
            data.append(site)
            continue
        site[4] = True

        # A mutation site must have two alleles, and enough depths and quality on the site
        if tB[1].size == 1 or not alt_depth:
            data.append(site)
            continue
        mut = tB[0][np.where(tB[1]==1)][0]
        index = np.where(B==mut)[0][0]
        if QUAL == NA or QUAL < minQUAL or GQ[index] < minGQ or v.format('DP')[index] < minDPm:
            data.append(site)
            continue

        # Mutation must not be polymorphic
        freq = dq[index]
        if freq < HQ and freq > LQ:
            data.append(site)
            continue
        # Verify that mutation frequency is high in only one line, and low in the remaining lines
        high = functools.reduce(lambda count, x: count + (x>HQ), dq, 0)
        low  = functools.reduce(lambda count, x: count + (x<LQ), dq, 0)
        if not high == 1 and not low == 1:
            data.append(site)
            continue
        site[5] = True
        site[6] = LINES[index]

        # Exclude INDELS?
        isINDEL = False
        if len(v.REF) > 1 or any(i > 1 for i in [len(i) for i in v.ALT]): isINDEL = True
        if excludeINDELS=='true' and isINDEL:
            data.append(site)
            continue
        elif excludeINDELS=='context' and isINDEL and len(data) > context:
            count_indel = context
            L = len(data)
            c = 1
            while context >= c:
                if data[L-c][0] != CHROM: break
                elif POS - data[L-c][1] > context: break
                else:
                    data[-c][4] = False
                    data[-c][5] = False
                    c = c+1
            data.append(site)
            continue
        
        data.append(site)
        
    df = pandas.DataFrame(data, columns=['CHROM', 'POS', 'REF', 'ALT', 'CALL', 'MUT', 'LINE', 'QUAL', 'MQ', 'DP', 'GQ'])
    if out is None: out = os.path.splitext(vcfile)[0] + str('_findmut.tsv')
    df.to_csv(out, sep='\t', encoding='utf-8', index=False)

def getAll (vcfile):

    HERE = os.path.dirname(__file__)
    VCF_PATH = os.path.join(HERE, vcfile)
    vcf = cyvcf2.VCF(VCF_PATH)

    # Get all sites, and save data from them
    chrom = []
    pos = []
    ref = []
    alt = []
    qual = []
    mq = []
    dp = []
    first = True
    wmode = 'w'
    for v in vcf:

        if len(chrom)>0 and chrom[-1] != v.CHROM:
            db = {'CHROM': chrom,
                    'POS': pos,
                    'REF': ref,
                    'ALT': alt,
                    'QUAL': qual,
                    'MQ': mq,
                    'DP': dp
                    }
           
            df = pandas.DataFrame(db, columns = ['CHROM','POS','REF','ALT','QUAL','MQ','DP'])
            df.to_csv('~/all.tsv', sep='\t', encoding='utf-8', index=False, header=first, mode=wmode)
            first = False
            wmode = 'a'
            chrom.clear()
            pos.clear()
            ref.clear()
            alt.clear()
            qual.clear()
            mq.clear()
            dp.clear()

        chrom.append(v.CHROM)
        pos.append(v.POS)
        ref.append(v.REF)
        if len(v.ALT) > 0: alt.append(v.ALT[0])
        else: alt.append(None)
        qual.append(v.QUAL)
        mq.append(v.INFO.get('MQ'))
        dp.append(v.INFO.get('DP'))
    
    db = {'CHROM': chrom,
        'POS': pos,
        'REF': ref,
        'ALT': alt,
        'QUAL': qual,
        'MQ': mq,
        'DP': dp
        }
    df = pandas.DataFrame(db, columns = ['CHROM','POS','REF','ALT','QUAL','MQ','DP'])
    df.to_csv('~/all.csv', sep=',', encoding='utf-8', index=False, header=first, mode=wmode)


def findContext (sites, vcfile, context=10, out=None):

    df = pandas.read_csv(sites)
    HERE = os.path.dirname(__file__)
    VCF_PATH = os.path.join(HERE, vcfile)
    vcf = cyvcf2.VCF(VCF_PATH)
    for index, row in df.iterrows():
        CHR = df.loc[index, 'CHROM']
        POS = df.loc[index, 'POS']
        window = str(CHR) + ':' + str(POS-context) + '-' + str(POS+context)
        up = ''
        down = ''
        for v in vcf(window):
            if v.POS < POS:
                if len(v.REF)==1: up = up + v.REF
                else: up = up + v.ALT[0]
            elif v.POS > POS:
                if len(v.REF)==1: down = down + v.REF
                else: down = down + v.ALT[0]
        df.loc[index, 'up'] = up
        df.loc[index, 'down'] = down
    if out is None: out = os.path.splitext(sites)[0] + str('_context.tsv')
    df.to_csv(out, sep='\t', encoding='utf-8', index=False)
