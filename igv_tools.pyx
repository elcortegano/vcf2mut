import os
import pandas

# This function reads a dataframe <data> with at least two columns: CHROM and POS
# It will also open an IGV XML <session> file with a reference genome and BAM files loaded
# For each position in <data>, a snapshot will be generated for the given region and a
# number <context> of pair bases at each side.
def snapshot(data, session, outputdir, igvpath='/opt/igv/igv.sh', context=10, sep=","):
    df = pandas.read_csv(data, sep=sep)
    f = open('script.bat', 'w')
    f.write('new\n')
    f.write('load ' + session + '\n')
    f.write('snapshotDirectory ' + outputdir + '\n')
    for index, row in df.iterrows():
        low = row['POS'] - context
        if low < 0: low = 0
        high = row['POS'] + context
        f.write('goto ' + str(row['CHROM']) + ':' + str(low) + '-' + str(high) + '\n')
        f.write('sort base\n')
        f.write('collapse\n')
        f.write('snapshot\n')
    
    f.write('exit\n')
    f.close()
    os.system('xvfb-run  --auto-servernum ' + igvpath + ' -b  script.bat')
