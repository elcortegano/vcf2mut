import numpy as np
import pandas as pd
import pickle
import os
import sys

# This function creates a CSV database containing a dictionary fast recovery of
# genomic features (together with their start and end positions) given their
# chromosome number

def getDictDB (gff, out = None):
    
    # Read gff file and create a hashmap for <key=sequence, value=dataframe<start,end,feature>>
    maptof = {}
    with open (gff, "r") as f:
        for line in f.readlines():
            li  = line.lstrip()
            if not li.startswith('#') and '\t' in li:
                l = line.split('\t')
                df = pd.DataFrame(columns=['START','END','FEATURE'], data=[[l[3],l[4],l[2]]])
                df = df.astype(dtype={'START':'uint32', 'END':'uint32', 'FEATURE':'str'})
                if l[0] in maptof:
                    maptof[l[0]] = maptof[l[0]].append(df)
                else:
                    maptof[l[0]] = df

    # Save dictionary as a Pickle database
    if out is None: out = os.path.splitext(gff)[0] + str('.pkl')
    w = open(out, "wb")
    pickle.dump(maptof, w)
    w.close()

def getFeature (sites, gffdb, out = None):

    # Load gff database
    with open(gffdb, 'rb') as h:
        db = pickle.load(h)

    # Read sites file (CSV format)
    df = pd.read_csv(sites)
    for index, row in df.iterrows():
        if row['CHROM'] in db:
            tmp = db[row['CHROM']]
            tmp = tmp[tmp['END']>row['POS']]
            tmp = tmp[tmp['START']<row['POS']]
            tmp = tmp.set_index([pd.Series([i for i in range(0,len(tmp))])])
            if len(tmp) == 0:
                df.loc[index, 'high'] = 'NA'
                df.loc[index, 'low'] = 'NA'
            else:
                df.loc[index, 'high'] = favoriteFeature(tmp['FEATURE'], hierarchy='high')
                df.loc[index, 'low'] = favoriteFeature(tmp['FEATURE'], hierarchy='low')
        else:
            df.loc[index, 'high'] = 'NA'
            df.loc[index, 'low'] = 'NA'

    if out is None: out = os.path.splitext(sites)[0] + str('_feature.csv')
    df.to_csv(out, sep=',', encoding='utf-8', index=False)

def favoriteFeature (v, hierarchy):

    if hierarchy is not 'low' and hierarchy is not 'high': sys.exit('feature hierarchy must be set to \'low\' or \'high\'')
    if hierarchy is 'high' and 'gene' in v.to_numpy(): return 'gene'
    elif 'similarity' in v.to_numpy(): return 'similarity'
    else:
        if 'CDS' in v.to_numpy(): return 'CDS'
        elif 'five_prime_utr' in v.to_numpy(): return 'five_prime_utr'
        elif 'three_prime_utr' in v.to_numpy(): return 'three_prime_utr'
        elif 'intron' in v.to_numpy(): return 'intron'
        elif 'gene' in v.to_numpy(): return 'gene'
        else: sys.exit('undefined feature found')
