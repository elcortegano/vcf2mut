import cyvcf2
import numpy as np
import os
import pandas as pd
import functools
import string

# Convert nessie Entropy and Linguistic files to CSV
def getEL (ELfile, col = 'E', lag = 0, out = None):

    def chomp(x):
        if x.endswith("\r\n"): return x[:-2]
        if x.endswith("\n") or x.endswith("\r"): return x[:-1]
        return x

    chrom = []
    pos = []
    EL = []
    tmp = pd.DataFrame()
    NAs = []
    if lag > 0:
        NAs = ['NA'] * lag
    if out is None: out = os.path.splitext(ELfile)[0] + str('.csv')
    if os.path.isfile(out): os.remove(out)
    with open (ELfile, "r") as f:
        c = '' # chromosome ID
        for line in f.readlines():
            li  = line.lstrip()
            if not li.startswith('#') and not li.startswith('@'):
                if li.startswith('>'):
                    li = li.rstrip()
                    c = li.replace('>','')
                    if len(EL) > 0:
                        if lag > 0:
                            if len(EL) <= (lag*2+1):
                                EL = ['NA'] * len(EL)
                            else:
                                EL = np.delete(EL, np.arange(len(EL)-lag*2, len(EL)))
                                EL = np.concatenate([NAs,EL, NAs])
                        tmp = pd.DataFrame(np.column_stack([chrom, pos, EL]), columns=['CHROM', 'POS', col])
                        if not os.path.isfile(out):
                            tmp.to_csv(out, sep='\t', encoding='utf-8', index=False)
                        else:
                            tmp.to_csv(out, sep='\t', encoding='utf-8', index=False, mode='a', header=False)
                        chrom = []
                        pos = []
                        EL = []
                else:
                    line = chomp(line)
                    l = line.split('\t')
                    chrom.append(c)
                    pos.append(int(l[0])+1)
                    EL.append(l[1])

    if len(EL) > 0:
        if lag > 0:
            if len(EL) <= (lag*2+1): EL = ['NA'] * len(EL)
            else:
                EL = np.delete(EL, np.arange(len(EL)-lag*2, len(EL)))
                EL = np.concatenate([NAs, EL, NAs])
        tmp = pd.DataFrame(np.column_stack([chrom, pos, EL]), columns=['CHROM', 'POS', col])

    tmp.to_csv(out, sep='\t', encoding='utf-8', index=False, mode='a', header=False)
